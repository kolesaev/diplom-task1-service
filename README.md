# Service

## Запуск из git-репозитория с Docker Compose
Для запуска сервиса из исходников Docker Compose размещённых в git-репозитории переййдите в папку git-docker-compose выполните следующую команду:
   ```shell
      ansible-playbook docker-compose-service.yaml -i "<path to inventory file>" --extra-vars "git_repositiry=<https git repository address>"
   ``` 
или для конкретного коммита:
   ```shell
      ansible-playbook docker-compose-service.yaml -i "<path to inventory file>" --extra-vars "git_repositiry=<https git repository address> git_commit=<commit hash>"
   ``` 